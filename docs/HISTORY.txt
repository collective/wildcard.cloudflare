Changelog
=========

1.0b2 (2015-02-09)
------------------

- add invalidate button
  [vangheem]

1.0b1 (2015-02-05)
------------------

- avoid // in urls
  [vangheem]

1.0a3 (2014-09-15)
------------------

- fix not being able to use http, https only schemes


1.0a2 (2014-05-07)
------------------

- spelling

1.0a1 (2014-05-02)
------------------

- Initial release
